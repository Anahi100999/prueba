import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HolaComponent } from './hola/hola.component';
import { AboutComponent } from './about/about.component';


const routes: Routes = [
  {path:'', component: HolaComponent},
  {path:'about', component: AboutComponent},
  {path:'**',redirectTo:'',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
