import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


link:string[] = ['faq','home', 'Services', 'About', 'contact'];
   
@Input() campany:string;
@Output() sendAlert = new EventEmitter();

showAlert(event) {
  this.sendAlert.emit(event);
  }
  constructor() { }

  ngOnInit(): void {
  }

}
